/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

import java.math.BigDecimal;
import java.math.BigInteger;

// TODO: Auto-generated Javadoc
/**
 * The Interface BigNumberIfc.
 */
public abstract interface BigNumberIfc {

	/**
	 * Doit.
	 *
	 * @param paramArrayOfString the param array of string
	 * @return the string
	 */
	public abstract String execute(String[] paramArrayOfString);

	/**
	 * Gets the big decimal.
	 *
	 * @param <T>    the generic type
	 * @param paramT the param T
	 * @return the big decimal
	 */
	public abstract <T> BigDecimal getBigDecimal(T paramT);

	/**
	 * Gets the big integer.
	 *
	 * @param <T>    the generic type
	 * @param paramT the param T
	 * @return the big integer
	 */
	public abstract <T> BigInteger getBigInteger(T paramT);

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public abstract String getOperation();

	/**
	 * Gets the return engineering string.
	 *
	 * @return the return engineering string
	 */
	public abstract boolean getReturnEngineeringString();

	/**
	 * The Class OperationConst.
	 */
	public static class OperationConst {

		/** The op type. */
		private int opType;

		/** The op. */
		private String op;

		/**
		 * Instantiates a new operation const.
		 *
		 * @param opType the op type
		 * @param op     the op
		 */
		public OperationConst(int opType, String op) {
			this.opType = opType;
			this.op = op;
		}

		/**
		 * Equals.
		 *
		 * @param obj the obj
		 * @return true, if successful
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			return String.valueOf(this.opType + "#" + this.op.toLowerCase())
					.equalsIgnoreCase(String.valueOf(obj).toLowerCase());
		}

		/**
		 * Gets the op.
		 *
		 * @return the op
		 */
		public String getOp() {
			return this.op;
		}

		/**
		 * Gets the op type.
		 *
		 * @return the op type
		 */
		public int getOpType() {
			return this.opType;
		}

		/**
		 * To string.
		 *
		 * @return the string
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return this.opType + "#" + this.op;
		}
	}
}
