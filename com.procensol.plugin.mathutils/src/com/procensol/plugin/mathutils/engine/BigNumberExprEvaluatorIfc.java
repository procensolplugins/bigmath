/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

// TODO: Auto-generated Javadoc
/**
 * The Interface BigNumberExprEvaluatorIfc.
 */
public abstract interface BigNumberExprEvaluatorIfc {

	/**
	 * Evaluate.
	 *
	 * @param <T>         the generic type
	 * @param paramString the param string
	 * @return the t
	 */
	public abstract <T> T evaluate(String paramString);

}
