/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface DecimalIfc.
 */
public abstract interface DecimalIfc extends BigNumberIfc {

	/**
	 * Plus.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String plus(List<T> paramList, boolean paramBoolean);

	/**
	 * Precision.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String precision(List<T> paramList, boolean paramBoolean);

	/**
	 * Round.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String round(List<T> paramList, boolean paramBoolean);

	/**
	 * To engineering string.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String toEngineeringString(List<T> paramList, boolean paramBoolean);

	/**
	 * To plain string.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String toPlainString(List<T> paramList, boolean paramBoolean);
}
