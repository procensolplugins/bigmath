/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface CommonIfc.
 */
public abstract interface CommonIfc extends BigNumberIfc {

	/**
	 * Abs.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String abs(List<T> paramList, boolean paramBoolean);

	/**
	 * Adds the.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String add(List<T> paramList, boolean paramBoolean);

	/**
	 * Byte value exact.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String byteValueExact(List<T> paramList, boolean paramBoolean);

	/**
	 * Compare to.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String compareTo(List<T> paramList, boolean paramBoolean);

	/**
	 * Divide.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String divide(List<T> paramList, boolean paramBoolean);

	/**
	 * Divide and remainder.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String divideAndRemainder(List<T> paramList, boolean paramBoolean);

	/**
	 * Divide to integral value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String divideToIntegralValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Double value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String doubleValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Equals.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String equals(List<T> paramList, boolean paramBoolean);

	/**
	 * Float value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String floatValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Hashcode.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String hashcode(List<T> paramList, boolean paramBoolean);

	/**
	 * Int value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String intValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Int value exact.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String intValueExact(List<T> paramList, boolean paramBoolean);

	/**
	 * Long value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String longValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Long value exact.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String longValueExact(List<T> paramList, boolean paramBoolean);

	/**
	 * Max.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String max(List<T> paramList, boolean paramBoolean);

	/**
	 * Min.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String min(List<T> paramList, boolean paramBoolean);

	/**
	 * Move point left.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String movePointLeft(List<T> paramList, boolean paramBoolean);

	/**
	 * Move point right.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String movePointRight(List<T> paramList, boolean paramBoolean);

	/**
	 * Multiply.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String multiply(List<T> paramList, boolean paramBoolean);

	/**
	 * Negate.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String negate(List<T> paramList, boolean paramBoolean);

	/**
	 * Percent is.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String percentIs(List<T> paramList, boolean paramBoolean);

	/**
	 * Percent of.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String percentOf(List<T> paramList, boolean paramBoolean);

	/**
	 * Pow.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String pow(List<T> paramList, boolean paramBoolean);

	/**
	 * Remainder.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String remainder(List<T> paramList, boolean paramBoolean);

	/**
	 * Scale.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String scale(List<T> paramList, boolean paramBoolean);

	/**
	 * Scale by power often.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String scaleByPowerOften(List<T> paramList, boolean paramBoolean);

	/**
	 * Sets the scale.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String setScale(List<T> paramList, boolean paramBoolean);

	/**
	 * Short value exact.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String shortValueExact(List<T> paramList, boolean paramBoolean);

	/**
	 * Signum.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String signum(List<T> paramList, boolean paramBoolean);

	/**
	 * Strip trailing zeros.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String stripTrailingZeros(List<T> paramList, boolean paramBoolean);

	/**
	 * Subtract.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String subtract(List<T> paramList, boolean paramBoolean);

	/**
	 * To big integer.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String toBigInteger(List<T> paramList, boolean paramBoolean);

	/**
	 * To big integer exact.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String toBigIntegerExact(List<T> paramList, boolean paramBoolean);

	/**
	 * To string.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String toString(List<T> paramList, boolean paramBoolean);

	/**
	 * Ulp.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String ulp(List<T> paramList, boolean paramBoolean);

	/**
	 * Un scaled value.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String unScaledValue(List<T> paramList, boolean paramBoolean);

	/**
	 * Value of.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String valueOf(List<T> paramList, boolean paramBoolean);
}
