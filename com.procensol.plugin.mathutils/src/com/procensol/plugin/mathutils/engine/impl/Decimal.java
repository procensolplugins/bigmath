/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import com.procensol.plugin.mathutils.utils.ConstantIfc;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Decimal.
 */
public class Decimal extends Common {

	/**
	 * Instantiates a new decimal.
	 *
	 * @param operation               the operation
	 * @param returnEngineeringString the return engineering string
	 */
	public Decimal(String operation, Boolean returnEngineeringString) {
		super(operation, returnEngineeringString);
	}

	/**
	 * Plus.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String plus(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_DECIMAL,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return Utils.convertBigNumberToString(bigNumber.plus(), this.returnEngineeringString.booleanValue());
	}

	/**
	 * Precision.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String precision(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_DECIMAL,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return String.valueOf(bigNumber.precision());
	}

	/**
	 * Round.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String round(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_DECIMAL, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigDecimal = getBigDecimal(numbersStrList.get(0));
		return Utils.convertBigNumberToString(bigDecimal.round(MathContext.UNLIMITED),
				this.returnEngineeringString.booleanValue());
	}

	/**
	 * To engineering string.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String toEngineeringString(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_DECIMAL, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return bigNumber.toEngineeringString();
	}

	/**
	 * To plain string.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String toPlainString(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_DECIMAL,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return bigNumber.toPlainString();
	}
}
