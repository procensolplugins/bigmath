/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.engine.BigNumberIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class BigNumber.
 */
public class BigNumber implements BigNumberIfc {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(BigNumber.class);

	/** The operation. */
	public String operation = null;

	/** The return engineering string. */
	public Boolean returnEngineeringString = Boolean.valueOf(false);

	/**
	 * Instantiates a new big number.
	 *
	 * @param operation               the operation
	 * @param returnEngineeringString the return engineering string
	 */
	public BigNumber(String operation, Boolean returnEngineeringString) {
		this.operation = operation;
		this.returnEngineeringString = returnEngineeringString;
	}

	/**
	 * Doit.
	 *
	 * @param clazz        the clazz
	 * @param operandsList the operands list
	 * @return the string
	 */
	public String execute(Class<? extends BigNumber> clazz, String[] operandsList) {
		
		List<String> bigNumbersList = Arrays.asList(operandsList);
		try {
			return (String) clazz.getMethod(this.operation, new Class[] { List.class, Boolean.TYPE }).invoke(this,
					new Object[] { bigNumbersList, this.returnEngineeringString });
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			LOG.error(e);
		}
		return "Not a valid operation, please check documentation.";
	}

	/**
	 * Execute.
	 *
	 * @param operandsList the operands list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.BigNumberIfc#doit(java.lang.String[])
	 */
	public String execute(String[] operandsList) {
		return execute(getClass(), operandsList);
	}

	/**
	 * Gets the big decimal.
	 *
	 * @param <T> the generic type
	 * @param number the number
	 * @return the big decimal
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.BigNumberIfc#getBigDecimal(java.lang.
	 * Object)
	 */
	public <T> BigDecimal getBigDecimal(T number) {
		return new BigDecimal(String.valueOf(number), MathContext.UNLIMITED);
	}

	/**
	 * Gets the big integer.
	 *
	 * @param <T> the generic type
	 * @param number the number
	 * @return the big integer
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.BigNumberIfc#getBigInteger(java.lang.
	 * Object)
	 */
	public <T> BigInteger getBigInteger(T number) {
		return new BigInteger(String.valueOf(number));
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.BigNumberIfc#getOperation()
	 */
	public String getOperation() {
		return this.operation;
	}

	/**
	 * Gets the return engineering string.
	 *
	 * @return the return engineering string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.BigNumberIfc#getReturnEngineeringString
	 * ()
	 */
	public boolean getReturnEngineeringString() {
		return this.returnEngineeringString.booleanValue();
	}
}
