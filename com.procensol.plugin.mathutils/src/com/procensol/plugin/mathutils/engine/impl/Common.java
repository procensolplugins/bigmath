/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.List;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.engine.CommonIfc;
import com.procensol.plugin.mathutils.utils.ConstantIfc;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Common.
 */
public class Common extends BigNumber implements CommonIfc {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Common.class);

	/**
	 * Instantiates a new common.
	 *
	 * @param operation               the operation
	 * @param returnEngineeringString the return engineering string
	 */
	public Common(String operation, Boolean returnEngineeringString) {
		super(operation, returnEngineeringString);
	}

	/**
	 * Abs.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#abs(java.util.List,
	 * boolean)
	 */
	public <T> String abs(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return Utils.convertBigNumberToString(bigNumber.abs(), this.returnEngineeringString.booleanValue());
	}

	/**
	 * Adds the.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#add(java.util.List,
	 * boolean)
	 */
	public <T> String add(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.TWO_OR_MORE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumberToReturn = new BigDecimal(0);
		for (T number : numbersStrList) {
			BigDecimal bigNumber = getBigDecimal(number);
			bigNumberToReturn = bigNumberToReturn.add(bigNumber);
		}
		return Utils.convertBigNumberToString(bigNumberToReturn, this.returnEngineeringString.booleanValue());
	}

	/**
	 * Byte value exact.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#byteValueExact(java.util.
	 * List, boolean)
	 */
	public <T> String byteValueExact(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

			return String.valueOf(bigNumber.byteValueExact());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Compare to.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#compareTo(java.util.List,
	 * boolean)
	 */
	public <T> String compareTo(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			BigDecimal compareWith = getBigDecimal(numbersStrList.get(1));

			return String.valueOf(bigNumber.compareTo(compareWith));
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Divide.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#divide(java.util.List,
	 * boolean)
	 */
	public <T> String divide(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal dividend = getBigDecimal(numbersStrList.get(0));
			BigDecimal divisor = getBigDecimal(numbersStrList.get(1));
			return Utils.convertBigNumberToString(dividend.divide(divisor, MathContext.DECIMAL128),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			e.printStackTrace();
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Divide and remainder.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#divideAndRemainder(java.util.
	 * List, boolean)
	 */
	public <T> String divideAndRemainder(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal dividend = getBigDecimal(numbersStrList.get(0));

			BigDecimal divisor = getBigDecimal(numbersStrList.get(1));

			BigDecimal[] results = dividend.divideAndRemainder(divisor);

			return results[0] + "#" + results[1];
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Divide to integral value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#divideToIntegralValue(java.
	 * util.List, boolean)
	 */
	public <T> String divideToIntegralValue(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Double value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#doubleValue(java.util.List,
	 * boolean)
	 */
	public <T> String doubleValue(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

			return String.valueOf(bigNumber.doubleValue());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Equals.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#equals(java.util.List,
	 * boolean)
	 */
	public <T> String equals(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			BigDecimal equqlsTo = getBigDecimal(numbersStrList.get(1));
			boolean result = bigNumber.equals(equqlsTo);

			return result ? "True" : "False";
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Float value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#floatValue(java.util.List,
	 * boolean)
	 */
	public <T> String floatValue(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.floatValue());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Hashcode.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#hashcode(java.util.List,
	 * boolean)
	 */
	public <T> String hashcode(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.hashCode());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Int value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#intValue(java.util.List,
	 * boolean)
	 */
	public <T> String intValue(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.intValue());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Int value exact.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#intValueExact(java.util.List,
	 * boolean)
	 */
	public <T> String intValueExact(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.intValueExact());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Long value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#longValue(java.util.List,
	 * boolean)
	 */
	public <T> String longValue(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.longValue());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Long value exact.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#longValueExact(java.util.
	 * List, boolean)
	 */
	public <T> String longValueExact(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));
			return String.valueOf(bigNumber.longValueExact());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Max.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#max(java.util.List,
	 * boolean)
	 */
	public <T> String max(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.TWO_OR_MORE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal max = new BigDecimal(0);
		for (T number : numbersStrList) {
			BigDecimal bigNumber = getBigDecimal(number);
			max = max.max(bigNumber);
		}
		return Utils.convertBigNumberToString(max, this.returnEngineeringString.booleanValue());
	}

	/**
	 * Min.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#min(java.util.List,
	 * boolean)
	 */
	public <T> String min(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH, numbersStrList.size(), isBigDecimalList,
				ConstantIfc.TWO_OR_MORE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal min = getBigDecimal(numbersStrList.get(0));
		for (T number : numbersStrList) {
			BigDecimal bigNumber = getBigDecimal(number);

			min = min.min(bigNumber);
		}
		return Utils.convertBigNumberToString(min, this.returnEngineeringString.booleanValue());
	}

	/**
	 * Move point left.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#movePointLeft(java.util.List,
	 * boolean)
	 */
	public <T> String movePointLeft(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Move point right.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#movePointRight(java.util.
	 * List, boolean)
	 */
	public <T> String movePointRight(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Multiply.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#multiply(java.util.List,
	 * boolean)
	 */
	public <T> String multiply(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumberToReturn = new BigDecimal(1);
		for (T number : numbersStrList) {
			BigDecimal bigNumber = getBigDecimal(number);

			bigNumberToReturn = bigNumberToReturn.multiply(bigNumber);
		}
		return Utils.convertBigNumberToString(bigNumberToReturn, this.returnEngineeringString.booleanValue());
	}

	/**
	 * Negate.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#negate(java.util.List,
	 * boolean)
	 */
	public <T> String negate(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return Utils.convertBigNumberToString(bigNumber.negate(), this.returnEngineeringString.booleanValue());
	}

	/**
	 * Percent is.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#percentIs(java.util.List,
	 * boolean)
	 */
	public <T> String percentIs(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal whole = getBigDecimal(numbersStrList.get(0));
			BigDecimal part = getBigDecimal(numbersStrList.get(1));
			BigDecimal result = part.multiply(whole).divide(getBigDecimal("100"));
			return Utils.convertBigNumberToString(result, this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Percent of.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#percentOf(java.util.List,
	 * boolean)
	 */
	public <T> String percentOf(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal part = getBigDecimal(numbersStrList.get(0));
			BigDecimal whole = getBigDecimal(numbersStrList.get(1));
			BigDecimal result = part.divide(whole).multiply(getBigDecimal("100"));
			return Utils.convertBigNumberToString(result, this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Pow.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#pow(java.util.List,
	 * boolean)
	 */
	public <T> String pow(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal base = getBigDecimal(numbersStrList.get(0));

			int exponent = (numbersStrList.get(1) instanceof BigInteger) ?

					getBigInteger(numbersStrList.get(1)).intValue() :

					getBigDecimal(numbersStrList.get(1)).intValue();

			return Utils.convertBigNumberToString(base.pow(exponent), this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Remainder.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#remainder(java.util.List,
	 * boolean)
	 */
	public <T> String remainder(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal dividend = getBigDecimal(numbersStrList.get(0));

			BigDecimal divisor = getBigDecimal(numbersStrList.get(1));

			return Utils.convertBigNumberToString(dividend.remainder(divisor),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Scale.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#scale(java.util.List,
	 * boolean)
	 */
	public <T> String scale(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Scale by power often.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#scaleByPowerOften(java.util.
	 * List, boolean)
	 */
	public <T> String scaleByPowerOften(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Sets the scale.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#setScale(java.util.List,
	 * boolean)
	 */
	public <T> String setScale(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Short value exact.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#shortValueExact(java.util.
	 * List, boolean)
	 */
	public <T> String shortValueExact(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

			return String.valueOf(bigNumber.shortValueExact());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Signum.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#signum(java.util.List,
	 * boolean)
	 */
	public <T> String signum(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

		return String.valueOf(bigNumber.signum());
	}

	/**
	 * Strip trailing zeros.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#stripTrailingZeros(java.util.
	 * List, boolean)
	 */
	public <T> String stripTrailingZeros(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Subtract.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#subtract(java.util.List,
	 * boolean)
	 */
	public <T> String subtract(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.TWO_OR_MORE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		BigDecimal bigNumberToReturn = getBigDecimal(numbersStrList.get(0));
		for (int i = 1; i < numbersStrList.size(); i++) {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(i));

			bigNumberToReturn = bigNumberToReturn.subtract(bigNumber);
		}
		return Utils.convertBigNumberToString(bigNumberToReturn, this.returnEngineeringString.booleanValue());
	}

	/**
	 * To big integer.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#toBigInteger(java.util.List,
	 * boolean)
	 */
	public <T> String toBigInteger(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * To big integer exact.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#toBigIntegerExact(java.util.
	 * List, boolean)
	 */
	public <T> String toBigIntegerExact(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * To string.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#toString(java.util.List,
	 * boolean)
	 */
	public <T> String toString(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_BOTH,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigDecimal bigNumber = getBigDecimal(numbersStrList.get(0));

			return Utils.convertBigNumberToString(bigNumber, this.returnEngineeringString.booleanValue());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Ulp.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#ulp(java.util.List,
	 * boolean)
	 */
	public <T> String ulp(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Un scaled value.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.engine.CommonIfc#unScaledValue(java.util.List,
	 * boolean)
	 */
	public <T> String unScaledValue(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}

	/**
	 * Value of.
	 *
	 * @param <T> the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.procensol.plugin.mathutils.engine.CommonIfc#valueOf(java.util.List,
	 * boolean)
	 */
	public <T> String valueOf(List<T> numbersStrList, boolean isBigDecimalList) {
		return "Not yet implemented.";
	}
}
