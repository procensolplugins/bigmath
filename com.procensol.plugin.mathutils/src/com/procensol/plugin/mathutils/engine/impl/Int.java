/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.utils.ConstantIfc;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Int.
 */
public class Int extends Common {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Int.class);

	/**
	 * Instantiates a new int.
	 *
	 * @param operation               the operation
	 * @param returnEngineeringString the return engineering string
	 */
	public Int(String operation, Boolean returnEngineeringString) {
		super(operation, returnEngineeringString);
	}

	/**
	 * And.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String and(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger andWith = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.and(andWith), this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * And not.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String andNot(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger andNotWith = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.andNot(andNotWith),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Bit count.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String bitCount(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return String.valueOf(bigNumber.bitCount());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Bit length.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String bitLength(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return String.valueOf(bigNumber.bitLength());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Clear bit.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String clearBit(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger nthBit = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.clearBit(nthBit.intValue()),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Flip bit.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String flipBit(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger nthBit = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.flipBit(nthBit.intValue()),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Gcd.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String gcd(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			return Utils.convertBigNumberToString(
					getBigInteger(numbersStrList.get(0)).gcd(getBigInteger(numbersStrList.get(1))),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Gets the lowest set bit.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the lowest set bit
	 */
	public <T> String getLowestSetBit(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return String.valueOf(bigNumber.getLowestSetBit());
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Checks if is probable prime.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String isProbablePrime(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			boolean result = bigNumber.isProbablePrime(1);

			return result ? "True" : "False";
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Mod.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String mod(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger mod = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.mod(mod), this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Mod inverse.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String modInverse(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger mod = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.modInverse(mod),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Mod pow.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String modPow(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_THREE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger exponent = getBigInteger(numbersStrList.get(1));

			BigInteger mod = getBigInteger(numbersStrList.get(2));

			return Utils.convertBigNumberToString(bigNumber.modPow(exponent, mod),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Next probablep prime.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String nextProbablepPrime(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return Utils.convertBigNumberToString(bigNumber.nextProbablePrime(),
					this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Not.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String not(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return Utils.convertBigNumberToString(bigNumber.not(), this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Or.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String or(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger orWith = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.and(orWith), this.returnEngineeringString.booleanValue());
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Probable prime.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String probablePrime(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bitLength = getBigInteger(numbersStrList.get(0));

			return Utils.convertBigNumberToString(BigInteger.probablePrime(bitLength.intValue(), new Random()));
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Sets the bit.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String setBit(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger nthBit = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.setBit(nthBit.intValue()));
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Shift left.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String shiftLeft(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger n = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.shiftLeft(n.intValue()));
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Shift right.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String shiftRight(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger n = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.shiftRight(n.intValue()));
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Test bit.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String testBit(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger nthBit = getBigInteger(numbersStrList.get(1));

			boolean result = bigNumber.testBit(nthBit.intValue());

			return result ? "True" : "False";
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Tobyte array.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String tobyteArray(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_ONE_PARAM_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			return bigNumber.toByteArray().toString();
		} catch (Exception e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}

	/**
	 * Xor.
	 *
	 * @param <T>              the generic type
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public <T> String xor(List<T> numbersStrList, boolean isBigDecimalList) {
		String validationMsg = Utils.validate(ConstantIfc.OP_TYPE_INT,

				numbersStrList.size(),

				isBigDecimalList,

				ConstantIfc.EXACT_TWO_PARMA_REQ);
		if (validationMsg != null) {
			return validationMsg;
		}
		try {
			BigInteger bigNumber = getBigInteger(numbersStrList.get(0));

			BigInteger xorWith = getBigInteger(numbersStrList.get(1));

			return Utils.convertBigNumberToString(bigNumber.and(xorWith));
		} catch (ArithmeticException e) {
			return Utils.buildException(LOG, e, e.getMessage());
		}
	}
}
