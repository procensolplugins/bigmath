/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine.impl;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;

import com.procensol.plugin.mathutils.bodmas.evaluator.AbstractEvaluator;
import com.procensol.plugin.mathutils.bodmas.helper.BracketPair;
import com.procensol.plugin.mathutils.bodmas.helper.Operator;
import com.procensol.plugin.mathutils.bodmas.helper.Parameters;
import com.procensol.plugin.mathutils.engine.BigNumberExprEvaluatorIfc;
import com.procensol.plugin.mathutils.engine.BigNumberIfc;
import com.procensol.plugin.mathutils.processer.BigNumberProcesser;

// TODO: Auto-generated Javadoc
/**
 * The Class BigNumberExprEvaluator.
 */
@SuppressWarnings("unchecked")
public class BigNumberExprEvaluator extends AbstractEvaluator<String> implements BigNumberExprEvaluatorIfc {

	/**
	 * The Enum Style.
	 */
	public static enum Style {

		/** The standard. */
		STANDARD;
	}

	/** The sum processer. */
	public static BigNumberIfc sumProcesser = BigNumberProcesser.getProcesser("add", false);

	/** The subtract processer. */
	public static BigNumberIfc subtractProcesser = BigNumberProcesser.getProcesser("subtract", false);

	/** The multiply processer. */
	public static BigNumberIfc multiplyProcesser = BigNumberProcesser.getProcesser("multiply", false);

	/** The divide processer. */
	public static BigNumberIfc divideProcesser = BigNumberProcesser.getProcesser("divide", false);

	/** The exponenet processer. */
	public static BigNumberIfc exponenetProcesser = BigNumberProcesser.getProcesser("pow", false);

	/** The percent of processer. */
	public static BigNumberIfc percentOfProcesser = BigNumberProcesser.getProcesser("percentIs", false);

	/** The Constant MINUS. */
	public static final Operator MINUS = new Operator("-", 2, Operator.Associativity.LEFT, 1);

	/** The Constant PLUS. */
	public static final Operator PLUS = new Operator("+", 2, Operator.Associativity.LEFT, 1);

	/** The Constant MULTIPLY. */
	public static final Operator MULTIPLY = new Operator("*", 2, Operator.Associativity.LEFT, 2);

	/** The Constant DIVIDE. */
	public static final Operator DIVIDE = new Operator("/", 2, Operator.Associativity.LEFT, 2);

	/** The Constant EXPONENT. */
	public static final Operator EXPONENT = new Operator("^", 2, Operator.Associativity.LEFT, 4);

	/** The Constant PERCENTOF. */
	public static final Operator PERCENTOF = new Operator("%", 2, Operator.Associativity.LEFT, 2);

	/** The Constant OPERATORS. */
	private static final Operator[] OPERATORS = { MINUS, PLUS, MULTIPLY, DIVIDE, EXPONENT, PERCENTOF };

	/** The default parameters. */
	private static Parameters DEFAULT_PARAMETERS;

	/** The Constant FORMATTER. */
	private static final ThreadLocal<NumberFormat> FORMATTER = new ThreadLocal() {
		protected NumberFormat initialValue() {
			return NumberFormat.getNumberInstance(Locale.US);
		}
	};

	/**
	 * Gets the default parameters.
	 *
	 * @return the default parameters
	 */
	public static Parameters getDefaultParameters() {
		return getDefaultParameters(Style.STANDARD);
	}

	/**
	 * Gets the default parameters.
	 *
	 * @param style the style
	 * @return the default parameters
	 */
	public static Parameters getDefaultParameters(Style style) {
		Parameters result = new Parameters();
		result.addOperators(Arrays.asList(OPERATORS));
		result.addExpressionBracket(BracketPair.PARENTHESES);
		return result;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	private static Parameters getParameters() {
		if (DEFAULT_PARAMETERS == null) {
			DEFAULT_PARAMETERS = getDefaultParameters();
		}
		return DEFAULT_PARAMETERS;
	}

	/**
	 * Instantiates a new big number expr evaluator.
	 */
	public BigNumberExprEvaluator() {
		this(getParameters());
	}

	/**
	 * Instantiates a new big number expr evaluator.
	 *
	 * @param parameters the parameters
	 */
	public BigNumberExprEvaluator(Parameters parameters) {
		super(parameters);
	}

	/**
	 * Evaluate.
	 *
	 * @param operator the operator
	 * @param operands the operands
	 * @param evaluationContext the evaluation context
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.bodmos.evaluator.AbstractEvaluator#evaluate(
	 * com.procensol.plugin.mathutils.bodmos.helper.Operator, java.util.Iterator,
	 * java.lang.Object)
	 */
	public String evaluate(Operator operator, Iterator<String> operands, Object evaluationContext) {
		String[] bigNumbersStrArr = { (String) operands.next(), (String) operands.next() };
		if (MINUS.equals(operator)) {
			return subtractProcesser.execute(bigNumbersStrArr);
		}
		if (PLUS.equals(operator)) {
			return sumProcesser.execute(bigNumbersStrArr);
		}
		if (MULTIPLY.equals(operator)) {
			return multiplyProcesser.execute(bigNumbersStrArr);
		}
		if (DIVIDE.equals(operator)) {
			return divideProcesser.execute(bigNumbersStrArr);
		}
		if (EXPONENT.equals(operator)) {
			return exponenetProcesser.execute(bigNumbersStrArr);
		}
		if (PERCENTOF.equals(operator)) {
			return percentOfProcesser.execute(bigNumbersStrArr);
		}
		return (String) super.evaluate(operator, operands, evaluationContext);
	}

	/**
	 * To value.
	 *
	 * @param literal the literal
	 * @param evaluationContext the evaluation context
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.bodmos.evaluator.AbstractEvaluator#toValue(
	 * java.lang.String, java.lang.Object)
	 */
	protected String toValue(String literal, Object evaluationContext) {
		ParsePosition p = new ParsePosition(0);
		Number result = ((NumberFormat) FORMATTER.get()).parse(literal, p);
		if ((p.getIndex() == 0) || (p.getIndex() != literal.length())) {
			throw new IllegalArgumentException(literal + " is not a number");
		}
		return result.toString();
	}
}
