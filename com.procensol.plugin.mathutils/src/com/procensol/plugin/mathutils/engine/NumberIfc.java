/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

import com.procensol.plugin.mathutils.utils.ConstantIfc;

// TODO: Auto-generated Javadoc
/**
 * The Interface NumberIfc.
 */
public abstract interface NumberIfc extends CommonIfc, DecimalIfc, IntegerIfc {

	/** The Constant ABS. */
	public static final BigNumberIfc.OperationConst ABS = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.ABS);

	/** The Constant ADD. */
	public static final BigNumberIfc.OperationConst ADD = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.ADD);

	/** The Constant BYTEVALUEEXACT. */
	public static final BigNumberIfc.OperationConst BYTEVALUEEXACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.BYTEVALUEEXACT);

	/** The Constant COMPARETO. */
	public static final BigNumberIfc.OperationConst COMPARETO = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.COMPARETO);

	/** The Constant DIVIDE. */
	public static final BigNumberIfc.OperationConst DIVIDE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.DIVIDE);

	/** The Constant DIVIDEANDREMAINDER. */
	public static final BigNumberIfc.OperationConst DIVIDEANDREMAINDER = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.DIVIDEANDREMAINDER);

	/** The Constant DIVIDETOINTEGRALVALUE. */
	public static final BigNumberIfc.OperationConst DIVIDETOINTEGRALVALUE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.DIVIDETOINTEGRALVALUE);

	/** The Constant DOUBLEVALUE. */
	public static final BigNumberIfc.OperationConst DOUBLEVALUE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.DOUBLEVALUE);

	/** The Constant EQUALS. */
	public static final BigNumberIfc.OperationConst EQUALS = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.EQUALS);

	/** The Constant FLOATVALUE. */
	public static final BigNumberIfc.OperationConst FLOATVALUE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.FLOATVALUE);

	/** The Constant HASHCODE. */
	public static final BigNumberIfc.OperationConst HASHCODE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.HASHCODE);

	/** The Constant INTVALUE. */
	public static final BigNumberIfc.OperationConst INTVALUE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.INTVALUE);

	/** The Constant INTVALUEEXACT. */
	public static final BigNumberIfc.OperationConst INTVALUEEXACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.INTVALUEEXACT);

	/** The Constant LONGVALUE. */
	public static final BigNumberIfc.OperationConst LONGVALUE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.LONGVALUE);

	/** The Constant LONGVALUEEXACT. */
	public static final BigNumberIfc.OperationConst LONGVALUEEXACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.LONGVALUEEXACT);

	/** The Constant MAX. */
	public static final BigNumberIfc.OperationConst MAX = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.MAX);

	/** The Constant MIN. */
	public static final BigNumberIfc.OperationConst MIN = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.MIN);

	/** The Constant MOVEPOINTLEFT. */
	public static final BigNumberIfc.OperationConst MOVEPOINTLEFT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.MOVEPOINTLEFT);

	/** The Constant MOVEPOINTRIGHT. */
	public static final BigNumberIfc.OperationConst MOVEPOINTRIGHT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.MOVEPOINTRIGHT);

	/** The Constant MULTIPLY. */
	public static final BigNumberIfc.OperationConst MULTIPLY = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.MULTIPLY);

	/** The Constant NEGATE. */
	public static final BigNumberIfc.OperationConst NEGATE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.NEGATE);

	/** The Constant POW. */
	public static final BigNumberIfc.OperationConst POW = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.POW);

	/** The Constant PERCENTIS. */
	public static final BigNumberIfc.OperationConst PERCENTIS = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.PERCENTIS);

	/** The Constant PERCENTOF. */
	public static final BigNumberIfc.OperationConst PERCENTOF = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.PERCENTOF);

	/** The Constant REMAINDER. */
	public static final BigNumberIfc.OperationConst REMAINDER = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.REMAINDER);

	/** The Constant SCALE. */
	public static final BigNumberIfc.OperationConst SCALE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.SCALE);

	/** The Constant SCALEBYPOWEROFTEN. */
	public static final BigNumberIfc.OperationConst SCALEBYPOWEROFTEN = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.SCALEBYPOWEROFTEN);

	/** The Constant SETSCALE. */
	public static final BigNumberIfc.OperationConst SETSCALE = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.SETSCALE);

	/** The Constant SHORTVALUEEXACT. */
	public static final BigNumberIfc.OperationConst SHORTVALUEEXACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.SHORTVALUEEXACT);

	/** The Constant SIGNUM. */
	public static final BigNumberIfc.OperationConst SIGNUM = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.SIGNUM);

	/** The Constant STRIPTRAILINGZEROS. */
	public static final BigNumberIfc.OperationConst STRIPTRAILINGZEROS = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.STRIPTRAILINGZEROS);

	/** The Constant SUBTRACT. */
	public static final BigNumberIfc.OperationConst SUBTRACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_DECIMAL, ConstantIfc.SUBTRACT);

	/** The Constant TOBIGINTEGER. */
	public static final BigNumberIfc.OperationConst TOBIGINTEGER = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.TOBIGINTEGER);

	/** The Constant TOBIGINTEGEREXACT. */
	public static final BigNumberIfc.OperationConst TOBIGINTEGEREXACT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.TOBIGINTEGEREXACT);

	/** The Constant TOSTRING. */
	public static final BigNumberIfc.OperationConst TOSTRING = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.TOSTRING);

	/** The Constant ULP. */
	public static final BigNumberIfc.OperationConst ULP = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.ULP);

	/** The Constant UNSCALEDVALUE. */
	public static final BigNumberIfc.OperationConst UNSCALEDVALUE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_BOTH, ConstantIfc.UNSCALEDVALUE);

	/** The Constant VALUEOF. */
	public static final BigNumberIfc.OperationConst VALUEOF = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_BOTH,
			ConstantIfc.VALUEOF);

	/** The Constant PLUS. */
	public static final BigNumberIfc.OperationConst PLUS = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_DECIMAL,
			ConstantIfc.PLUS);

	/** The Constant PRECISION. */
	public static final BigNumberIfc.OperationConst PRECISION = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_DECIMAL, ConstantIfc.PRECISION);

	/** The Constant ROUND. */
	public static final BigNumberIfc.OperationConst ROUND = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_DECIMAL,
			ConstantIfc.ROUND);

	/** The Constant TOENGINEERINGSTRING. */
	public static final BigNumberIfc.OperationConst TOENGINEERINGSTRING = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_DECIMAL, ConstantIfc.TOENGINEERINGSTRING);

	/** The Constant TOPLAINSTRING. */
	public static final BigNumberIfc.OperationConst TOPLAINSTRING = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_DECIMAL, ConstantIfc.TOPLAINSTRING);

	/** The Constant AND. */
	public static final BigNumberIfc.OperationConst AND = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.AND);

	/** The Constant ANDNOT. */
	public static final BigNumberIfc.OperationConst ANDNOT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.ANDNOT);

	/** The Constant BITCOUNT. */
	public static final BigNumberIfc.OperationConst BITCOUNT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.BITCOUNT);

	/** The Constant BITLENGTH. */
	public static final BigNumberIfc.OperationConst BITLENGTH = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.BITLENGTH);

	/** The Constant CLEARBIT. */
	public static final BigNumberIfc.OperationConst CLEARBIT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.CLEARBIT);

	/** The Constant FLIPBIT. */
	public static final BigNumberIfc.OperationConst FLIPBIT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.FLIPBIT);

	/** The Constant GCD. */
	public static final BigNumberIfc.OperationConst GCD = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.GCD);

	/** The Constant GETLOWESTSETBIT. */
	public static final BigNumberIfc.OperationConst GETLOWESTSETBIT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.GETLOWESTSETBIT);

	/** The Constant ISPROBABLEPRIME. */
	public static final BigNumberIfc.OperationConst ISPROBABLEPRIME = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.ISPROBABLEPRIME);

	/** The Constant MOD. */
	public static final BigNumberIfc.OperationConst MOD = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.MOD);

	/** The Constant MODINVERSE. */
	public static final BigNumberIfc.OperationConst MODINVERSE = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.MODINVERSE);

	/** The Constant MODPOW. */
	public static final BigNumberIfc.OperationConst MODPOW = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.MODPOW);

	/** The Constant NEXTPROBABLEPRIME. */
	public static final BigNumberIfc.OperationConst NEXTPROBABLEPRIME = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.NEXTPROBABLEPRIME);

	/** The Constant NOT. */
	public static final BigNumberIfc.OperationConst NOT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.NOT);

	/** The Constant OR. */
	public static final BigNumberIfc.OperationConst OR = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.OR);

	/** The Constant PROBABLEPRIME. */
	public static final BigNumberIfc.OperationConst PROBABLEPRIME = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.PROBABLEPRIME);

	/** The Constant SETBIT. */
	public static final BigNumberIfc.OperationConst SETBIT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.SETBIT);

	/** The Constant SHIFTLEFT. */
	public static final BigNumberIfc.OperationConst SHIFTLEFT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.SHIFTLEFT);

	/** The Constant SHIFTRIGHT. */
	public static final BigNumberIfc.OperationConst SHIFTRIGHT = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.SHIFTRIGHT);

	/** The Constant TESTBIT. */
	public static final BigNumberIfc.OperationConst TESTBIT = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.TESTBIT);

	/** The Constant TOBYTEARRAY. */
	public static final BigNumberIfc.OperationConst TOBYTEARRAY = new BigNumberIfc.OperationConst(
			ConstantIfc.OP_TYPE_INT, ConstantIfc.TOBYTEARRAY);

	/** The Constant XOR. */
	public static final BigNumberIfc.OperationConst XOR = new BigNumberIfc.OperationConst(ConstantIfc.OP_TYPE_INT,
			ConstantIfc.XOR);
}
