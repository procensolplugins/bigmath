/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.engine;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface IntegerIfc.
 */
public abstract interface IntegerIfc extends BigNumberIfc {

	/**
	 * And.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String and(List<T> paramList, boolean paramBoolean);

	/**
	 * And not.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String andNot(List<T> paramList, boolean paramBoolean);

	/**
	 * Bit count.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String bitCount(List<T> paramList, boolean paramBoolean);

	/**
	 * Bit length.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String bitLength(List<T> paramList, boolean paramBoolean);

	/**
	 * Clear bit.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String clearBit(List<T> paramList, boolean paramBoolean);

	/**
	 * Flip bit.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String flipBit(List<T> paramList, boolean paramBoolean);

	/**
	 * Gcd.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String gcd(List<T> paramList, boolean paramBoolean);

	/**
	 * Gets the lowest set bit.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the lowest set bit
	 */
	public abstract <T> String getLowestSetBit(List<T> paramList, boolean paramBoolean);

	/**
	 * Checks if is probable prime.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String isProbablePrime(List<T> paramList, boolean paramBoolean);

	/**
	 * Mod.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String mod(List<T> paramList, boolean paramBoolean);

	/**
	 * Mod inverse.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String modInverse(List<T> paramList, boolean paramBoolean);

	/**
	 * Mod pow.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String modPow(List<T> paramList, boolean paramBoolean);

	/**
	 * Next probablep prime.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String nextProbablepPrime(List<T> paramList, boolean paramBoolean);

	/**
	 * Not.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String not(List<T> paramList, boolean paramBoolean);

	/**
	 * Or.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String or(List<T> paramList, boolean paramBoolean);

	/**
	 * Probable prime.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String probablePrime(List<T> paramList, boolean paramBoolean);

	/**
	 * Sets the bit.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String setBit(List<T> paramList, boolean paramBoolean);

	/**
	 * Shift left.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String shiftLeft(List<T> paramList, boolean paramBoolean);

	/**
	 * Shift right.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String shiftRight(List<T> paramList, boolean paramBoolean);

	/**
	 * Test bit.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String testBit(List<T> paramList, boolean paramBoolean);

	/**
	 * Tobyte array.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String tobyteArray(List<T> paramList, boolean paramBoolean);

	/**
	 * Xor.
	 *
	 * @param <T>          the generic type
	 * @param paramList    the param list
	 * @param paramBoolean the param boolean
	 * @return the string
	 */
	public abstract <T> String xor(List<T> paramList, boolean paramBoolean);
}
