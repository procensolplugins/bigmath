/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.test;

import com.procensol.plugin.mathutils.engine.BigNumberExprEvaluatorIfc;
import com.procensol.plugin.mathutils.processer.BigNumberProcesser;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpressionTester.
 */
public class ExpressionTester {

	/**
	 * The main method.
	 *
	 * @param str the arguments
	 */
	public static void main(String[] str) {
		String expr = "(123213234323232.987+(23432143214321/3243241324132.345435%9390849048354958043985.989))";
		BigNumberExprEvaluatorIfc bn = BigNumberProcesser.getEvaluator(Boolean.valueOf(false));

		System.out.println(bn.evaluate(expr).toString());
	}
}
