/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.test;

import java.util.ArrayList;

import com.procensol.plugin.mathutils.processer.BigNumberProcesser;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationUnitTest.
 */
public class OperationUnitTest {

	/**
	 * The main method.
	 *
	 * @param asdf the arguments
	 */
	public static void main(String[] asdf) {
		ArrayList<String> test = new ArrayList();
		test.add("abc");
		test.add("abcA");
		test.add("abcB");
		System.out.println(test.contains("abca"));

		String[] oprand = new String[] { "123.00", "23.99" };

		String bn = BigNumberProcesser.getProcesser("divide", true).execute(oprand);
		System.out.println(bn);
	}
}
