/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.procensol.plugin.mathutils.engine.BigNumberIfc;
import com.procensol.plugin.mathutils.processer.BigNumberProcesser;
import com.procensol.plugin.mathutils.utils.ConstantIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class ExhaustiveTest.
 */
public class ExhaustiveTest {

	/** The ret eng string. */
	public static boolean retEngString = true;

	/**
	 * The main method.
	 *
	 * @param er the arguments
	 */
	public static void main(String[] er) {
		Integer scale = Integer.valueOf(12);

		String[] allOps = { "ABS", "ADD", "AND", "ANDNOT", "BITCOUNT", "BITLENGTH", "BYTEVALUEEXACT", "CLEARBIT",
				"COMPARETO", "DIVIDE", "DIVIDEANDREMAINDER", "DIVIDETOINTEGRALVALUE", "DOUBLEVALUE", "EQUALS",
				"FLIPBIT", "FLOATVALUE", "GCD", "GETLOWESTSETBIT", "HASHCODE", "INTVALUE", "INTVALUEEXACT",
				"ISPROBABLEPRIME", "LONGVALUE", "LONGVALUEEXACT", "MAX", "MIN", "MOD", "MODINVERSE", "MODPOW",
				"MOVEPOINTLEFT", "MOVEPOINTRIGHT", "MULTIPLY", "NEGATE", "NEXTPROBABLEPRIME", "NOT", "OR", "PERCENTIS",
				"PERCENTOF", "PLUS", "POW", "PRECISION", "PROBABLEPRIME", "REMAINDER", "ROUND", "SCALE",
				"SCALEBYPOWEROFTEN", "SETBIT", "SETSCALE", "SHIFTLEFT", "SHIFTRIGHT", "SHORTVALUEEXACT", "SIGNUM",
				"STRIPTRAILINGZEROS", "SUBTRACT", "TESTBIT", "TOBIGINTEGER", "TOBIGINTEGEREXACT", "TOBYTEARRAY",
				"TOENGINEERINGSTRING", "TOPLAINSTRING", "TOSTRING", "ULP", "UNSCALEDVALUE", "VALUEOF", "XOR" };

		String[][] testParamsArrDecimal = { { "12345678998754332.99999999" },

				{ "12345678998754332.99999999", "98.99999999" },

				{ "12345678998754332.99999999", "98765432123456789.99", "56.99999999" },

				{ "12345678998754332.99999999", "12345678998754332.99999999", "56.99", "99.99999999" } };

		String[][] testParamsArrInteger = { { "12345678998754332" }, { "12345678998754332", "98" },
				{ "123", "9887686876786786", "56" }, { "123", "765665476567567576987", "987987897987", "999" } };

		String[][] testParamsArrMixed = { { "765665476567567576987.99999999" },
				{ "765665476567567576987", "98.99999999" }, { "123.234324324239", "98", "56.99999999" },
				{ "323432432.99999999", "98", "5674563464354.99999999", "99" } };

		System.out.println(1);

		HashMap<String, HashMap<Integer, ArrayList<String>>> allTestResutSet = new HashMap();

		allTestResutSet.put("INTEGER", runTestCase("INTEGER", allOps, testParamsArrInteger));
		allTestResutSet.put("DECIMAL", runTestCase("DECIMAL", allOps, testParamsArrDecimal));
		allTestResutSet.put("BOTH", runTestCase("BOTH", allOps, testParamsArrMixed));

		System.out.println(4);

		writeResultsToDisk(allTestResutSet);
	}

	/**
	 * Run test case.
	 *
	 * @param type      the type
	 * @param opArr     the op arr
	 * @param paramsArr the params arr
	 * @return the array list
	 */
	public static ArrayList<String> runTestCase(String type, String[] opArr, String[] paramsArr) {
		ArrayList<String> resultsList = new ArrayList();

		resultsList.add("Type\tOperation\t#Parameters passed\tParameters\tResults");
		for (String op : opArr) {
			BigNumberIfc bnp = BigNumberProcesser.getProcesser(op.toLowerCase(), true);
			try {
				String expr = "evaluatebigmath(" + op + ", " + "false, {\"" + String.join("\", \"", paramsArr) + "\"})";
				resultsList.add(type + "\t" + op + "\t" + paramsArr.length + "\t'" + String.join(", ", paramsArr) + "\t"
						+ expr + "\t'" + bnp.execute(paramsArr));
			} catch (Exception localException) {
			}
		}
		return resultsList;
	}

	/**
	 * Run test case.
	 *
	 * @param type      the type
	 * @param allOps    the all ops
	 * @param paramArrs the param arrs
	 * @return the hash map
	 */
	private static HashMap<Integer, ArrayList<String>> runTestCase(String type, String[] allOps, String[][] paramArrs) {
		HashMap<Integer, ArrayList<String>> parameterResutSet = new HashMap();
		int index = 1;
		for (String[] paramsArr : paramArrs) {
			parameterResutSet.put(Integer.valueOf(index++), runTestCase(type, allOps, paramsArr));
		}
		return parameterResutSet;
	}

	/**
	 * Write results to disk.
	 *
	 * @param allTestResutSet the all test resut set
	 */
	private static void writeResultsToDisk(HashMap<String, HashMap<Integer, ArrayList<String>>> allTestResutSet) {

		StringBuffer sb = new StringBuffer();

		Iterator<String> keyItr = allTestResutSet.keySet().iterator();

		while (keyItr.hasNext()) {

			String key = keyItr.next();

			HashMap<Integer, ArrayList<String>> parammeterResMap = allTestResutSet.get(key);

			Iterator<Integer> parammeterResMapKeyItr = parammeterResMap.keySet().iterator();

			while (parammeterResMapKeyItr.hasNext()) {

				Integer parammeterResMapKey = parammeterResMapKeyItr.next();

				ArrayList<String> resultsList = parammeterResMap.get(parammeterResMapKey);

				for (String string : resultsList) {

					sb.append(string).append(ConstantIfc.NEW_LINE);

				}

			}

		}

		PrintStream out;

		try {

			out = new PrintStream(new FileOutputStream("c:\\temp\\res.txt"));

			out.print(sb.toString());

		} catch (FileNotFoundException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		}

	}

}
