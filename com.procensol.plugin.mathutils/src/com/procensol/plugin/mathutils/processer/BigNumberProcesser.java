/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.processer;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.engine.BigNumberExprEvaluatorIfc;
import com.procensol.plugin.mathutils.engine.BigNumberIfc;
import com.procensol.plugin.mathutils.engine.impl.BigNumberExprEvaluator;
import com.procensol.plugin.mathutils.engine.impl.Common;
import com.procensol.plugin.mathutils.engine.impl.Decimal;
import com.procensol.plugin.mathutils.engine.impl.Int;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class BigNumberProcesser.
 */
public class BigNumberProcesser {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(BigNumberProcesser.class);

	/**
	 * Gets the evaluator.
	 *
	 * @param returnEngineeringString the return engineering string
	 * @return the evaluator
	 */
	public static BigNumberExprEvaluatorIfc getEvaluator(Boolean returnEngineeringString) {
		return new BigNumberExprEvaluator();
	}

	/**
	 * Gets the processer.
	 *
	 * @param op                      the op
	 * @param returnEngineeringString the return engineering string
	 * @return the processer
	 */
	public static BigNumberIfc getProcesser(String op, boolean returnEngineeringString) {
		op = op.toUpperCase();
		if (isCommonOperation(op)) {
			return new Common(Utils.properOperation(op), Boolean.valueOf(returnEngineeringString));
		}
		if (isDecimalOperation(op)) {
			return new Decimal(Utils.properOperation(op), Boolean.valueOf(returnEngineeringString));
		}
		if (isIntOpearion(op)) {
			return new Int(Utils.properOperation(op), Boolean.valueOf(returnEngineeringString));
		}
		return null;
	}

	/**
	 * Checks if is common operation.
	 *
	 * @param op the op
	 * @return true, if is common operation
	 */
	private static boolean isCommonOperation(String op) {
		return Utils.isType(0, op);
	}

	/**
	 * Checks if is decimal operation.
	 *
	 * @param op the op
	 * @return true, if is decimal operation
	 */
	private static boolean isDecimalOperation(String op) {
		return Utils.isType(1, op);
	}

	/**
	 * Checks if is int opearion.
	 *
	 * @param op the op
	 * @return true, if is int opearion
	 */
	private static boolean isIntOpearion(String op) {
		return Utils.isType(2, op);
	}

}
