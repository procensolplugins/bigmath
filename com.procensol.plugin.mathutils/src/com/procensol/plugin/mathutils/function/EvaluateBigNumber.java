/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.function;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
import com.procensol.plugin.mathutils.processer.BigNumberProcesser;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class EvaluateBigNumber.
 */
@Category("category.name.MathematicalFunctions")
public class EvaluateBigNumber {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(EvaluateBigNumber.class);

	/**
	 * Evaluatebigmath.
	 *
	 * @param sc                      the sc
	 * @param operation               the operation
	 * @param returnEngineeringString the return engineering string
	 * @param operandsList            the operands list
	 * @return the string
	 */
	@Function
	public String evaluatebigmath(ServiceContext sc, @Parameter(required = true) String operation,
			@Parameter(required = true) Boolean returnEngineeringString,
			@Parameter(unlimited = true, required = true) String[] operandsList) {
		try {
			return BigNumberProcesser.getProcesser(operation, returnEngineeringString.booleanValue())
					.execute(operandsList);
		} catch (Exception e) {
			LOG.error(e);
			return Utils.buildException(LOG, e, "Error occured while initializing");
		}
	}

}
