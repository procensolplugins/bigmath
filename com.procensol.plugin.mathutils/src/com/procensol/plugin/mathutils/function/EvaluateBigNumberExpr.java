/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.function;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
import com.procensol.plugin.mathutils.processer.BigNumberProcesser;
import com.procensol.plugin.mathutils.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class EvaluateBigNumberExpr.
 */
@Category("category.name.MathematicalFunctions")
public class EvaluateBigNumberExpr {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(EvaluateBigNumberExpr.class);

	/**
	 * Evaluatebignumberexpr.
	 *
	 * @param sc                      the sc
	 * @param expression              the expression
	 * @param returnEngineeringString the return engineering string
	 * @return the string
	 */
	@Function
	public String evaluatebignumberexpr(ServiceContext sc, @Parameter(required = true) String expression,
			@Parameter(required = true) Boolean returnEngineeringString) {
		try {
			return BigNumberProcesser.getEvaluator(returnEngineeringString).evaluate(expression).toString();
		} catch (Exception e) {
			LOG.error(e);
			return Utils.buildException(LOG, e, "Error occured while initializing");
		}
	}
}
