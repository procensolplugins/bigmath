/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Constant.
 */
public class Constant implements ConstantIfc {

	/** The name. */
	private String name;

	/**
	 * Instantiates a new constant.
	 *
	 * @param name the name
	 */
	public Constant(String name) {
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
}
