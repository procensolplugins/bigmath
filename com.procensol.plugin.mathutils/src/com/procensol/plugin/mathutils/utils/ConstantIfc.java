/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.utils;

// TODO: Auto-generated Javadoc
/**
 * The Interface ConstantIfc.
 */
public interface ConstantIfc {

	/** The Constant ERR_TXT_PREFIX. */
	public static final String ERR_TXT_PREFIX = "Error while performing ";

	/** The Constant VALIDATION_ERR_TXT_PREFIX. */
	public static final String VALIDATION_ERR_TXT_PREFIX = "Validation error while performing ";

	/** The Constant HYPHEN. */
	public static final String HYPHEN = " - ";

	/** The Constant DECIMAL. */
	public static final String DECIMAL = ".";

	/** The Constant COMMA. */
	public static final String COMMA = ", ";

	/** The Constant NEW_LINE. */
	public static final String NEW_LINE = "\n";

	/** The Constant TAB. */
	public static final String TAB = "\t";

	/** The Constant TRUE_STR. */
	public static final String TRUE_STR = "True";

	/** The Constant FALSE_STR. */
	public static final String FALSE_STR = "False";

	/** The Constant OP_TYPE_BOTH. */
	public static final int OP_TYPE_BOTH = 0;

	/** The Constant OP_TYPE_DECIMAL. */
	public static final int OP_TYPE_DECIMAL = 1;

	/** The Constant OP_TYPE_INT. */
	public static final int OP_TYPE_INT = 2;

	/** The Constant ABS. */
	public static final String ABS = "abs";

	/** The Constant ADD. */
	public static final String ADD = "add";

	/** The Constant BYTEVALUEEXACT. */
	public static final String BYTEVALUEEXACT = "byteValueExact";

	/** The Constant COMPARETO. */
	public static final String COMPARETO = "compareTo";

	/** The Constant DIVIDE. */
	public static final String DIVIDE = "divide";

	/** The Constant DIVIDEANDREMAINDER. */
	public static final String DIVIDEANDREMAINDER = "divideAndRemainder";

	/** The Constant DIVIDETOINTEGRALVALUE. */
	public static final String DIVIDETOINTEGRALVALUE = "divideToIntegralValue";

	/** The Constant DOUBLEVALUE. */
	public static final String DOUBLEVALUE = "doubleValue";

	/** The Constant EQUALS. */
	public static final String EQUALS = "equals";

	/** The Constant FLOATVALUE. */
	public static final String FLOATVALUE = "floatValue";

	/** The Constant HASHCODE. */
	public static final String HASHCODE = "hashcode";

	/** The Constant INTVALUE. */
	public static final String INTVALUE = "intValue";

	/** The Constant INTVALUEEXACT. */
	public static final String INTVALUEEXACT = "intValueExact";

	/** The Constant LONGVALUE. */
	public static final String LONGVALUE = "longValue";

	/** The Constant LONGVALUEEXACT. */
	public static final String LONGVALUEEXACT = "longValueExact";

	/** The Constant MAX. */
	public static final String MAX = "max";

	/** The Constant MIN. */
	public static final String MIN = "min";

	/** The Constant MOVEPOINTLEFT. */
	public static final String MOVEPOINTLEFT = "movePointLeft";

	/** The Constant MOVEPOINTRIGHT. */
	public static final String MOVEPOINTRIGHT = "movePointRight";

	/** The Constant MULTIPLY. */
	public static final String MULTIPLY = "multiply";

	/** The Constant NEGATE. */
	public static final String NEGATE = "negate";

	/** The Constant POW. */
	public static final String POW = "pow";

	/** The Constant PERCENTIS. */
	public static final String PERCENTIS = "percentIs";

	/** The Constant PERCENTOF. */
	public static final String PERCENTOF = "percentOf";

	/** The Constant REMAINDER. */
	public static final String REMAINDER = "remainder";

	/** The Constant SCALE. */
	public static final String SCALE = "scale";

	/** The Constant SCALEBYPOWEROFTEN. */
	public static final String SCALEBYPOWEROFTEN = "scaleByPowerOften";

	/** The Constant SETSCALE. */
	public static final String SETSCALE = "setScale";

	/** The Constant SHORTVALUEEXACT. */
	public static final String SHORTVALUEEXACT = "shortValueExact";

	/** The Constant SIGNUM. */
	public static final String SIGNUM = "signum";

	/** The Constant STRIPTRAILINGZEROS. */
	public static final String STRIPTRAILINGZEROS = "stripTrailingZeros";

	/** The Constant SUBTRACT. */
	public static final String SUBTRACT = "subtract";

	/** The Constant TOBIGINTEGER. */
	public static final String TOBIGINTEGER = "toBigInteger";

	/** The Constant TOBIGINTEGEREXACT. */
	public static final String TOBIGINTEGEREXACT = "toBigIntegerExact";

	/** The Constant TOSTRING. */
	public static final String TOSTRING = "toString";

	/** The Constant ULP. */
	public static final String ULP = "ulp";

	/** The Constant UNSCALEDVALUE. */
	public static final String UNSCALEDVALUE = "unScaledValue";

	/** The Constant VALUEOF. */
	public static final String VALUEOF = "valueOf";

	/** The Constant PLUS. */
	public static final String PLUS = "plus";

	/** The Constant PRECISION. */
	public static final String PRECISION = "precision";

	/** The Constant ROUND. */
	public static final String ROUND = "round";

	/** The Constant TOENGINEERINGSTRING. */
	public static final String TOENGINEERINGSTRING = "toEngineeringString";

	/** The Constant TOPLAINSTRING. */
	public static final String TOPLAINSTRING = "toPlainString";

	/** The Constant AND. */
	public static final String AND = "and";

	/** The Constant ANDNOT. */
	public static final String ANDNOT = "andNot";

	/** The Constant BITCOUNT. */
	public static final String BITCOUNT = "bitCount";

	/** The Constant BITLENGTH. */
	public static final String BITLENGTH = "bitLength";

	/** The Constant CLEARBIT. */
	public static final String CLEARBIT = "clearBit";

	/** The Constant FLIPBIT. */
	public static final String FLIPBIT = "flipBit";

	/** The Constant GCD. */
	public static final String GCD = "gcd";

	/** The Constant GETLOWESTSETBIT. */
	public static final String GETLOWESTSETBIT = "getLowestSetBit";

	/** The Constant ISPROBABLEPRIME. */
	public static final String ISPROBABLEPRIME = "isProbablePrime";

	/** The Constant MOD. */
	public static final String MOD = "mod";

	/** The Constant MODINVERSE. */
	public static final String MODINVERSE = "modInverse";

	/** The Constant MODPOW. */
	public static final String MODPOW = "modPow";

	/** The Constant NEXTPROBABLEPRIME. */
	public static final String NEXTPROBABLEPRIME = "nextProbablepPrime";

	/** The Constant NOT. */
	public static final String NOT = "not";

	/** The Constant OR. */
	public static final String OR = "or";

	/** The Constant PROBABLEPRIME. */
	public static final String PROBABLEPRIME = "probablePrime";

	/** The Constant SETBIT. */
	public static final String SETBIT = "setBit";

	/** The Constant SHIFTLEFT. */
	public static final String SHIFTLEFT = "shiftLeft";

	/** The Constant SHIFTRIGHT. */
	public static final String SHIFTRIGHT = "shiftRight";

	/** The Constant TESTBIT. */
	public static final String TESTBIT = "testBit";

	/** The Constant TOBYTEARRAY. */
	public static final String TOBYTEARRAY = "tobyteArray";

	/** The Constant XOR. */
	public static final String XOR = "xor";

	/** The Constant EXACT_ONE_PARAM_REQ. */
	public static final int EXACT_ONE_PARAM_REQ = 101;

	/** The Constant EXACT_TWO_PARMA_REQ. */
	public static final int EXACT_TWO_PARMA_REQ = 102;

	/** The Constant EXACT_THREE_PARAM_REQ. */
	public static final int EXACT_THREE_PARAM_REQ = 103;

	/** The Constant EXACT_FOUR_PARAM_REQ. */
	public static final int EXACT_FOUR_PARAM_REQ = 104;

	/** The Constant ONE_OR_MORE_PARAM_REQ. */
	public static final int ONE_OR_MORE_PARAM_REQ = 201;

	/** The Constant TWO_OR_MORE_PARAM_REQ. */
	public static final int TWO_OR_MORE_PARAM_REQ = 202;

	/** The Constant THREE_OR_MORE_PARAM_REQ. */
	public static final int THREE_OR_MORE_PARAM_REQ = 203;

	/** The Constant FOUR_OR_MORE_PARAM_REQ. */
	public static final int FOUR_OR_MORE_PARAM_REQ = 204;

	/** The Constant VAL_MSG_EXACT_ONE_NUM_REQ. */
	public static final String VAL_MSG_EXACT_ONE_NUM_REQ = "exactly one number required for this operation.";

	/** The Constant VAL_MSG_EXACT_TWO_NUM_REQ. */
	public static final String VAL_MSG_EXACT_TWO_NUM_REQ = "exactly two numbers required for this operation.";

	/** The Constant VAL_MSG_EXACT_THREE_NUM_REQ. */
	public static final String VAL_MSG_EXACT_THREE_NUM_REQ = "exactly three numbers required for this operation.";

	/** The Constant VAL_MSG_EXACT_FOUR_NUM_REQ. */
	public static final String VAL_MSG_EXACT_FOUR_NUM_REQ = "exactly four numbers required for this operation.";

	/** The Constant VAL_MSG_ONE_OR_MORE_NUM_REQ. */
	public static final String VAL_MSG_ONE_OR_MORE_NUM_REQ = "one more numbers required for this operation.";

	/** The Constant VAL_MSG_TWO_OR_MORE_NUM_REQ. */
	public static final String VAL_MSG_TWO_OR_MORE_NUM_REQ = "two or more numbers required for this operation.";

	/** The Constant VAL_MSG_THREE_OR_MORE_NUM_REQ. */
	public static final String VAL_MSG_THREE_OR_MORE_NUM_REQ = "three or more numbers required for this operation.";

	/** The Constant VAL_MSG_FOUR_OR_MORE_NUM_REQ. */
	public static final String VAL_MSG_FOUR_OR_MORE_NUM_REQ = "four or more numbers required for this operation.";

	/** The Constant VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT. */
	public static final String VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT = "this operation is applicable only on Integer value.";

	/** The Constant VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL. */
	public static final String VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL = "this operation is applicable only on Decimal value.";

	/** The Constant NOT_IMPLEMENTED_YET. */
	public static final String NOT_IMPLEMENTED_YET = "Not yet implemented.";
}
