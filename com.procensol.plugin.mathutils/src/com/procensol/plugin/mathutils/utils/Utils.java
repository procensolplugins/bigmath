/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.engine.BigNumberIfc;
import com.procensol.plugin.mathutils.engine.NumberIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class Utils.
 *
 * @author Ritesh
 */

public class Utils {

	/**
	 * Clean me.
	 *
	 * @param arr the arr
	 * @return the list
	 */
	public static List<String> cleanMe(String[] arr) {
		List<String> retrunList = new ArrayList<String>();

		return retrunList;
	}

	/**
	 * Builds the exception.
	 *
	 * @param log     the log
	 * @param e       the e
	 * @param message the message
	 * @return the string
	 */
	public static String buildException(Logger log, Exception e, String message) {
		log.error(e);
		return message.concat(" ").concat(e.getMessage());
	}

	/**
	 * Checks if is big decimal list.
	 *
	 * @param numbersStrList the numbers str list
	 * @return true, if is big decimal list
	 */
	public static boolean isBigDecimalList(List<String> numbersStrList) {
		for (String string : numbersStrList) {
			if (string.contains(".")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Convert string array to big numbers list.
	 *
	 * @param <T>            the generic type
	 * @param numbersStrList the numbers str list
	 * @param isBigDecimal   the is big decimal
	 * @return the list
	 */
	public static <T> List<T> convertStringArrayToBigNumbersList(List<String> numbersStrList, boolean isBigDecimal) {

		List<T> bigIntList = new ArrayList<>();

		List<T> bigDeciList = new ArrayList<>();

		for (String bigNumberStr : numbersStrList) {

			if (isBigDecimal) {

				bigDeciList.add((T) new BigDecimal(bigNumberStr));

			} else {

				bigIntList.add((T) new BigInteger(bigNumberStr));

			}

		}

		return isBigDecimal ? bigDeciList : bigIntList;

	}

	/**
	 * Convert big number to string.
	 *
	 * @param number                  the number
	 * @param returnEngineeringString the return engineering string
	 * @return the string
	 */
	public static String convertBigNumberToString(BigDecimal number, boolean returnEngineeringString) {
		System.out.println(number);
		try {
			return returnEngineeringString ? number.toEngineeringString() : number.toPlainString();
		} catch (Exception e) {
			return number.toString();
		}

	}

	/**
	 * Convert big number to string.
	 *
	 * @param number                  the number
	 * @param returnEngineeringString the return engineering string
	 * @return the string
	 */
	public static String convertBigNumberToString(BigInteger number, boolean returnEngineeringString) {
		return number.toString();
	}

	/**
	 * Convert big number to string.
	 *
	 * @param number the number
	 * @return the string
	 */
	public static String convertBigNumberToString(BigInteger number) {
		return number.toString();
	}

	/**
	 * Proper operation.
	 *
	 * @param op the op
	 * @return the string
	 */
	public static String properOperation(String op) {
		try {
			return ((BigNumberIfc.OperationConst) NumberIfc.class.getDeclaredField(op.toUpperCase()).get(op)).getOp();
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Validate.
	 *
	 * @param opType           the op type
	 * @param paramsLen        the params len
	 * @param isBigDecimalList the is big decimal list
	 * @param requiredParams   the required params
	 * @return the string
	 */
	public static String validate(int opType, int paramsLen, boolean isBigDecimalList, int requiredParams) {

		List<String> validationMsgsList = new ArrayList<>();

		if (requiredParams == ConstantIfc.EXACT_ONE_PARAM_REQ && paramsLen != 1)

			validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

		else if (requiredParams == ConstantIfc.EXACT_TWO_PARMA_REQ && paramsLen != 2)

			validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

		else if (requiredParams == ConstantIfc.EXACT_THREE_PARAM_REQ && paramsLen != 3)

			validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_THREE_NUM_REQ);

		else if (requiredParams == ConstantIfc.EXACT_FOUR_PARAM_REQ && paramsLen != 4)

			validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_FOUR_NUM_REQ);

		else if (requiredParams == ConstantIfc.ONE_OR_MORE_PARAM_REQ && paramsLen < 1)

			validationMsgsList.add(ConstantIfc.VAL_MSG_ONE_OR_MORE_NUM_REQ);

		else if (requiredParams == ConstantIfc.TWO_OR_MORE_PARAM_REQ && paramsLen < 2)

			validationMsgsList.add(ConstantIfc.VAL_MSG_TWO_OR_MORE_NUM_REQ);

		else if (requiredParams == ConstantIfc.THREE_OR_MORE_PARAM_REQ && paramsLen < 3)

			validationMsgsList.add(ConstantIfc.VAL_MSG_THREE_OR_MORE_NUM_REQ);

		else if (requiredParams == ConstantIfc.FOUR_OR_MORE_PARAM_REQ && paramsLen < 4)

			validationMsgsList.add(ConstantIfc.VAL_MSG_FOUR_OR_MORE_NUM_REQ);

		if (opType == ConstantIfc.OP_TYPE_INT && isBigDecimalList)

			validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

		if (opType == ConstantIfc.OP_TYPE_DECIMAL && !isBigDecimalList)

			validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

		return buildValidationErrorMsg(validationMsgsList);

	}

	/**
	 * Validate.
	 *
	 * @param op               the op
	 * @param numbersStrList   the numbers str list
	 * @param isBigDecimalList the is big decimal list
	 * @return the string
	 */
	public static String validate(String op, List<String> numbersStrList, boolean isBigDecimalList) {

		int paramsLen = numbersStrList.size();

		op = op.toUpperCase();

		List<String> validationMsgsList = new ArrayList<>();

		switch (op) {

		case ConstantIfc.ABS:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			break;

		case ConstantIfc.ADD:

			if (paramsLen < 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_TWO_OR_MORE_NUM_REQ);

			break;

		case ConstantIfc.AND:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.ANDNOT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.BITCOUNT:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.BITLENGTH:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.BYTEVALUEEXACT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.CLEARBIT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.COMPARETO:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.DIVIDE:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.DIVIDEANDREMAINDER:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.DIVIDETOINTEGRALVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.DOUBLEVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.EQUALS:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.FLIPBIT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.FLOATVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.GCD:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.GETLOWESTSETBIT:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.HASHCODE:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			break;

		case ConstantIfc.INTVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.INTVALUEEXACT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.ISPROBABLEPRIME:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.LONGVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.LONGVALUEEXACT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.MAX:

			if (paramsLen < 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_TWO_OR_MORE_NUM_REQ);

			break;

		case ConstantIfc.MIN:

			if (paramsLen < 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_TWO_OR_MORE_NUM_REQ);

			break;

		case ConstantIfc.MOD:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.MODINVERSE:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.MODPOW:

			if (paramsLen != 3)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_THREE_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.MOVEPOINTLEFT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.MOVEPOINTRIGHT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.MULTIPLY:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.NEGATE:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			break;

		case ConstantIfc.PLUS:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.NEXTPROBABLEPRIME:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.NOT:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.OR:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.POW:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.PRECISION:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.PROBABLEPRIME:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.REMAINDER:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			break;

		case ConstantIfc.SETBIT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.SHIFTLEFT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.SHIFTRIGHT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.SHORTVALUEEXACT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.ROUND:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.SCALE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.SCALEBYPOWEROFTEN:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.SETSCALE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.SIGNUM:

			if (paramsLen != 1)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_ONE_NUM_REQ);

			break;

		case ConstantIfc.STRIPTRAILINGZEROS:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.SUBTRACT:

			if (paramsLen < 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_TWO_OR_MORE_NUM_REQ);

			break;

		case ConstantIfc.TESTBIT:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.TOBIGINTEGER:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.TOBIGINTEGEREXACT:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.TOBYTEARRAY:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		case ConstantIfc.TOENGINEERINGSTRING:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.TOPLAINSTRING:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.TOSTRING:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.ULP:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.UNSCALEDVALUE:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			if (!isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGDECIMAL);

			break;

		case ConstantIfc.VALUEOF:

			validationMsgsList.add(ConstantIfc.NOT_IMPLEMENTED_YET);

			break;

		case ConstantIfc.XOR:

			if (paramsLen != 2)

				validationMsgsList.add(ConstantIfc.VAL_MSG_EXACT_TWO_NUM_REQ);

			if (isBigDecimalList)

				validationMsgsList.add(ConstantIfc.VAL_MSG_OP_APPLICABLE_ONLY_ON_BIGINT);

			break;

		}

		return buildValidationErrorMsg(validationMsgsList);

	}

	/**
	 * Builds the validation error msg.
	 *
	 * @param validationMsgsList the validation msgs list
	 * @return the string
	 */
	public static String buildValidationErrorMsg(List<String> validationMsgsList) {
		if ((validationMsgsList == null) || (validationMsgsList.size() == 0)) {
			return null;
		}
		StringBuffer sb = new StringBuffer("Validation error while performing ");

		sb.append(" - ");

		int index = 0;
		for (String msg : validationMsgsList) {
			sb.append(msg);

			index++;
			if (index != validationMsgsList.size()) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	/**
	 * Checks if is type.
	 *
	 * @param opType the op type
	 * @param op     the op
	 * @return true, if is type
	 */
	public static boolean isType(int opType, String op) {
		try {
			return NumberIfc.class.getDeclaredField(op).get(op).equals(String.valueOf(opType + "#" + op));

		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Utility function to check if a given character is an arithmetic operator.
	 *
	 * @param c the c
	 * @return true if operator, false if not
	 */
	public static boolean isAnOperator(char c) {
		return (c == '*' || c == '/' || c == '+' || c == '-' || c == '%');
	}

	/**
	 * Checks position and placement of (, ), and operators in a string to make sure
	 * it is a valid arithmetic expression.
	 *
	 * @param expression the expression
	 * @return true if the string is a valid arithmetic expression, false if not
	 */
	public static boolean isValidExpression(String expression) {
		expression = expression.replaceAll("\\s+", "");
		if (isAnOperator(expression.charAt(0)) || isAnOperator(expression.charAt(expression.length() - 1)))
			return false;

		int unclosedParenthesis = 0;

		for (int i = 0; i < expression.length(); i++) {
			if (expression.charAt(i) == '(') {
				unclosedParenthesis++;
				if (i == expression.length() - 1)
					return false;
			}
			if (expression.charAt(i) == ')') {
				unclosedParenthesis--;
				if (i == 0)
					return false;

			}
			if (isAnOperator(expression.charAt(i))) {
				if (expression.charAt(i - 1) == '(' || expression.charAt(i + 1) == ')'
						|| isAnOperator(expression.charAt(i + 1))) {
					return false;
				}

			}

		}
		return (unclosedParenthesis == 0);
	}

}
