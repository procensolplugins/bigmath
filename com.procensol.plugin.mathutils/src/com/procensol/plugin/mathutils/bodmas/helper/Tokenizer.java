/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class Tokenizer.
 */
public class Tokenizer {

	/** The pattern. */
	private Pattern pattern;

	/** The token delimiters. */
	private String tokenDelimiters;

	/** The trim tokens. */
	private boolean trimTokens;

	/**
	 * The Class StringTokenizerIterator.
	 */
	private class StringTokenizerIterator implements Iterator<String> {

		/** The tokens. */
		private StringTokenizer tokens;

		/** The next token. */
		private String nextToken = null;

		/**
		 * Instantiates a new string tokenizer iterator.
		 *
		 * @param tokens the tokens
		 */
		public StringTokenizerIterator(StringTokenizer tokens) {
			this.tokens = tokens;
		}

		/**
		 * Builds the next token.
		 *
		 * @return true, if successful
		 */
		private boolean buildNextToken() {
			while ((this.nextToken == null) && (this.tokens.hasMoreTokens())) {
				this.nextToken = this.tokens.nextToken();
				if (Tokenizer.this.trimTokens) {
					this.nextToken = this.nextToken.trim();
				}
				if (this.nextToken.isEmpty()) {
					this.nextToken = null;
				}
			}
			return this.nextToken != null;
		}

		/**
		 * Checks for next.
		 *
		 * @return true, if successful
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#hasNext()
		 */
		public boolean hasNext() {
			return buildNextToken();
		}

		/**
		 * Next.
		 *
		 * @return the string
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#next()
		 */
		public String next() {
			if (!buildNextToken()) {
				throw new NoSuchElementException();
			}
			String token = this.nextToken;
			this.nextToken = null;
			return token;
		}

		/**
		 * Removes the.
		 */
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#remove()
		 */
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Delimiters to regexp.
	 *
	 * @param delimiters the delimiters
	 * @return the pattern
	 */
	private static Pattern delimitersToRegexp(List<String> delimiters) {
		Collections.sort(delimiters, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return -o1.compareTo(o2);
			}
		});
		StringBuilder result = new StringBuilder();
		result.append('(');
		for (String delim : delimiters) {
			if (result.length() != 1) {
				result.append('|');
			}
			result.append("\\Q").append(delim).append("\\E");
		}
		result.append(')');
		return Pattern.compile(result.toString());
	}

	/**
	 * Instantiates a new tokenizer.
	 *
	 * @param delimiters the delimiters
	 */
	public Tokenizer(List<String> delimiters) {
		if (onlyOneChar(delimiters)) {
			StringBuilder builder = new StringBuilder();
			for (String delimiter : delimiters) {
				builder.append(delimiter);
			}
			this.tokenDelimiters = builder.toString();
		} else {
			this.pattern = delimitersToRegexp(delimiters);
		}
		this.trimTokens = true;
	}

	/**
	 * Adds the to tokens.
	 *
	 * @param tokens the tokens
	 * @param token  the token
	 */
	private void addToTokens(List<String> tokens, String token) {
		if (this.trimTokens) {
			token = token.trim();
		}
		if (!token.isEmpty()) {
			tokens.add(token);
		}
	}

	/**
	 * Checks if is trim tokens.
	 *
	 * @return true, if is trim tokens
	 */
	public boolean isTrimTokens() {
		return this.trimTokens;
	}

	/**
	 * Only one char.
	 *
	 * @param delimiters the delimiters
	 * @return true, if successful
	 */
	private boolean onlyOneChar(List<String> delimiters) {
		for (String delimiter : delimiters) {
			if (delimiter.length() != 1) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Sets the trim tokens.
	 *
	 * @param trimTokens the new trim tokens
	 */
	public void setTrimTokens(boolean trimTokens) {
		this.trimTokens = trimTokens;
	}

	/**
	 * Tokenize.
	 *
	 * @param string the string
	 * @return the iterator
	 */
	public Iterator<String> tokenize(String string) {
		if (this.pattern != null) {
			List<String> res = new ArrayList<String>();
			Matcher m = this.pattern.matcher(string);
			int pos = 0;
			while (m.find()) {
				if (pos != m.start()) {
					addToTokens(res, string.substring(pos, m.start()));
				}
				addToTokens(res, m.group());
				pos = m.end();
			}
			if (pos != string.length()) {
				addToTokens(res, string.substring(pos));
			}
			return res.iterator();
		}
		return new StringTokenizerIterator(new StringTokenizer(string, this.tokenDelimiters, true));
	}
}
