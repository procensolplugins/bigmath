/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

// TODO: Auto-generated Javadoc
/**
 * The Class BracketPair.
 */
public class BracketPair {

	/** The Constant PARENTHESES. */
	public static final BracketPair PARENTHESES = new BracketPair('(', ')');

	/** The Constant BRACKETS. */
	public static final BracketPair BRACKETS = new BracketPair('[', ']');

	/** The Constant BRACES. */
	public static final BracketPair BRACES = new BracketPair('{', '}');

	/** The Constant ANGLES. */
	public static final BracketPair ANGLES = new BracketPair('<', '>');

	/** The open. */
	private String open;

	/** The close. */
	private String close;

	/**
	 * Instantiates a new bracket pair.
	 *
	 * @param open  the open
	 * @param close the close
	 */
	public BracketPair(char open, char close) {
		this.open = new String(new char[] { open });
		this.close = new String(new char[] { close });
	}

	/**
	 * Gets the close.
	 *
	 * @return the close
	 */
	public String getClose() {
		return this.close;
	}

	/**
	 * Gets the open.
	 *
	 * @return the open
	 */
	public String getOpen() {
		return this.open;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.open + this.close;
	}
}
