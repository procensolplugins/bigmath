/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

// TODO: Auto-generated Javadoc
/**
 * The Class Operator.
 */
public class Operator {

	/** The symbol. */
	private String symbol;

	/** The precedence. */
	private int precedence;

	/** The operand count. */
	private int operandCount;

	/** The associativity. */
	private Associativity associativity;

	/**
	 * The Enum Associativity.
	 */
	public static enum Associativity {

		/** The left. */
		LEFT,
		/** The right. */
		RIGHT,
		/** The none. */
		NONE;
	}

	/**
	 * Instantiates a new operator.
	 *
	 * @param symbol        the symbol
	 * @param operandCount  the operand count
	 * @param associativity the associativity
	 * @param precedence    the precedence
	 */
	public Operator(String symbol, int operandCount, Associativity associativity, int precedence) {
		if ((symbol == null) || (associativity == null)) {
			throw new NullPointerException();
		}
		if (symbol.length() == 0) {
			throw new IllegalArgumentException("Operator symbol can't be null");
		}
		if ((operandCount < 1) || (operandCount > 2)) {
			throw new IllegalArgumentException("Only unary and binary operators are supported");
		}
		if (Associativity.NONE.equals(associativity)) {
			throw new IllegalArgumentException("None associativity operators are not supported");
		}
		this.symbol = symbol;
		this.operandCount = operandCount;
		this.associativity = associativity;
		this.precedence = precedence;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || ((obj instanceof Operator))) {
			return false;
		}
		Operator other = (Operator) obj;
		if ((this.operandCount != other.operandCount) || (this.associativity != other.associativity)) {
			return false;
		}
		if (this.symbol == null) {
			if (other.symbol != null) {
				return false;
			}
		} else if (!this.symbol.equals(other.symbol)) {
			return false;
		}
		if (this.precedence != other.precedence) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the associativity.
	 *
	 * @return the associativity
	 */
	public Associativity getAssociativity() {
		return this.associativity;
	}

	/**
	 * Gets the operand count.
	 *
	 * @return the operand count
	 */
	public int getOperandCount() {
		return this.operandCount;
	}

	/**
	 * Gets the precedence.
	 *
	 * @return the precedence
	 */
	public int getPrecedence() {
		return this.precedence;
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return this.symbol;
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = 31 * result + this.operandCount;
		result = 31 * result + (this.associativity == null ? 0 : this.associativity.hashCode());
		result = 31 * result + (this.symbol == null ? 0 : this.symbol.hashCode());
		result = 31 * result + this.precedence;
		return result;
	}
}
