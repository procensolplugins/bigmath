/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

// TODO: Auto-generated Javadoc
/**
 * The Class Token.
 */
public class Token {

	/** The kind. */
	private Kind kind;

	/** The content. */
	private Object content;

	/**
	 * The Enum Kind.
	 */
	private static enum Kind {

		/** The open bracket. */
		OPEN_BRACKET,
		/** The close bracket. */
		CLOSE_BRACKET,
		/** The operator. */
		OPERATOR,
		/** The literal. */
		LITERAL;
	}

	/**
	 * Builds the close token.
	 *
	 * @param pair the pair
	 * @return the token
	 */
	public static Token buildCloseToken(BracketPair pair) {
		return new Token(Kind.CLOSE_BRACKET, pair);
	}

	/**
	 * Builds the literal.
	 *
	 * @param literal the literal
	 * @return the token
	 */
	public static Token buildLiteral(String literal) {
		return new Token(Kind.LITERAL, literal);
	}

	/**
	 * Builds the open token.
	 *
	 * @param pair the pair
	 * @return the token
	 */
	public static Token buildOpenToken(BracketPair pair) {
		return new Token(Kind.OPEN_BRACKET, pair);
	}

	/**
	 * Builds the operator.
	 *
	 * @param ope the ope
	 * @return the token
	 */
	public static Token buildOperator(Operator ope) {
		return new Token(Kind.OPERATOR, ope);
	}

	/**
	 * Instantiates a new token.
	 *
	 * @param kind    the kind
	 * @param content the content
	 */
	private Token(Kind kind, Object content) {
		if (((kind.equals(Kind.OPERATOR)) && (!(content instanceof Operator)))
				|| ((kind.equals(Kind.LITERAL)) && (!(content instanceof String)))) {
			throw new IllegalArgumentException();
		}
		this.kind = kind;
		this.content = content;
	}

	/**
	 * Gets the associativity.
	 *
	 * @return the associativity
	 */
	public Operator.Associativity getAssociativity() {
		return getOperator().getAssociativity();
	}

	/**
	 * Gets the brackets.
	 *
	 * @return the brackets
	 */
	public BracketPair getBrackets() {
		return (BracketPair) this.content;
	}

	/**
	 * Gets the kind.
	 *
	 * @return the kind
	 */
	public Kind getKind() {
		return this.kind;
	}

	/**
	 * Gets the literal.
	 *
	 * @return the literal
	 */
	public String getLiteral() {
		if (!this.kind.equals(Kind.LITERAL)) {
			throw new IllegalArgumentException();
		}
		return (String) this.content;
	}

	/**
	 * Gets the operator.
	 *
	 * @return the operator
	 */
	public Operator getOperator() {
		return (Operator) this.content;
	}

	/**
	 * Gets the precedence.
	 *
	 * @return the precedence
	 */
	public int getPrecedence() {
		return getOperator().getPrecedence();
	}

	/**
	 * Checks if is close bracket.
	 *
	 * @return true, if is close bracket
	 */
	public boolean isCloseBracket() {
		return this.kind.equals(Kind.CLOSE_BRACKET);
	}

	/**
	 * Checks if is literal.
	 *
	 * @return true, if is literal
	 */
	public boolean isLiteral() {
		return this.kind.equals(Kind.LITERAL);
	}

	/**
	 * Checks if is open bracket.
	 *
	 * @return true, if is open bracket
	 */
	public boolean isOpenBracket() {
		return this.kind.equals(Kind.OPEN_BRACKET);
	}

	/**
	 * Checks if is operator.
	 *
	 * @return true, if is operator
	 */
	public boolean isOperator() {
		return this.kind.equals(Kind.OPERATOR);
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Token [kind=" + this.kind + ", content=" + this.content + "]";
	}
}
