/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.procensol.plugin.mathutils.utils.Constant;

// TODO: Auto-generated Javadoc
/**
 * The Class Parameters.
 */
public class Parameters {

	/** The operators. */
	private final List<Operator> operators;

	/** The constants. */
	private final List<Constant> constants;

	/** The translations. */
	private final Map<String, String> translations;

	/** The expression brackets. */
	private final List<BracketPair> expressionBrackets;

	/**
	 * Instantiates a new parameters.
	 */
	public Parameters() {
		this.operators = new ArrayList<Operator>();
		this.constants = new ArrayList<Constant>();
		this.translations = new HashMap<String, String>();
		this.expressionBrackets = new ArrayList<BracketPair>();
	}

	/**
	 * Adds the.
	 *
	 * @param constant the constant
	 */
	public void add(Constant constant) {
		this.constants.add(constant);
	}

	/**
	 * Adds the.
	 *
	 * @param operator the operator
	 */
	public void add(Operator operator) {
		this.operators.add(operator);
	}

	/**
	 * Adds the constants.
	 *
	 * @param constants the constants
	 */
	public void addConstants(Collection<Constant> constants) {
		this.constants.addAll(constants);
	}

	/**
	 * Adds the expression bracket.
	 *
	 * @param pair the pair
	 */
	public void addExpressionBracket(BracketPair pair) {
		this.expressionBrackets.add(pair);
	}

	/**
	 * Adds the expression brackets.
	 *
	 * @param brackets the brackets
	 */
	public void addExpressionBrackets(Collection<BracketPair> brackets) {
		this.expressionBrackets.addAll(brackets);
	}

	/**
	 * Adds the operators.
	 *
	 * @param operators the operators
	 */
	public void addOperators(Collection<Operator> operators) {
		this.operators.addAll(operators);
	}

	/**
	 * Gets the constants.
	 *
	 * @return the constants
	 */
	public Collection<Constant> getConstants() {
		return this.constants;
	}

	/**
	 * Gets the expression brackets.
	 *
	 * @return the expression brackets
	 */
	public Collection<BracketPair> getExpressionBrackets() {
		return this.expressionBrackets;
	}

	/**
	 * Gets the operators.
	 *
	 * @return the operators
	 */
	public Collection<Operator> getOperators() {
		return this.operators;
	}

	/**
	 * Gets the translation.
	 *
	 * @param originalName the original name
	 * @return the translation
	 */
	public String getTranslation(String originalName) {
		String translation = (String) this.translations.get(originalName);
		return translation == null ? originalName : translation;
	}

	/**
	 * Sets the translation.
	 *
	 * @param constant       the constant
	 * @param translatedName the translated name
	 */
	public void setTranslation(Constant constant, String translatedName) {
		setTranslation(constant.getName(), translatedName);
	}

	/**
	 * Sets the translation.
	 *
	 * @param name           the name
	 * @param translatedName the translated name
	 */
	private void setTranslation(String name, String translatedName) {
		this.translations.put(name, translatedName);
	}
}
