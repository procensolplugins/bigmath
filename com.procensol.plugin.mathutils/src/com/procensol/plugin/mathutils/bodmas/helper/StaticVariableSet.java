/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.helper;

import java.util.HashMap;
import java.util.Map;

import com.procensol.plugin.mathutils.bodmas.evaluator.AbstractVariableSet;

// TODO: Auto-generated Javadoc
/**
 * The Class StaticVariableSet.
 *
 * @param <T> the generic type
 */
public class StaticVariableSet<T> implements AbstractVariableSet<T> {

	/** The var to value. */
	private final Map<String, T> varToValue;

	/**
	 * Instantiates a new static variable set.
	 */
	public StaticVariableSet() {
		this.varToValue = new HashMap<String, T>();
	}

	/**
	 * Gets the.
	 *
	 * @param variableName the variable name
	 * @return the t
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.procensol.plugin.mathutils.bodmos.evaluator.AbstractVariableSet#get(java.
	 * lang.String)
	 */
	public T get(String variableName) {
		return this.varToValue.get(variableName);
	}

	/**
	 * Sets the.
	 *
	 * @param variableName the variable name
	 * @param value        the value
	 */
	public void set(String variableName, T value) {
		this.varToValue.put(variableName, value);
	}
}
