/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.evaluator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.procensol.plugin.mathutils.bodmas.helper.BracketPair;
import com.procensol.plugin.mathutils.bodmas.helper.Operator;
import com.procensol.plugin.mathutils.bodmas.helper.Parameters;
import com.procensol.plugin.mathutils.bodmas.helper.Token;
import com.procensol.plugin.mathutils.bodmas.helper.Tokenizer;
import com.procensol.plugin.mathutils.utils.Constant;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractEvaluator.
 *
 * @param <T> the generic type
 */
public abstract class AbstractEvaluator<T> {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(AbstractEvaluator.class);

	/** The tokenizer. */
	private final Tokenizer tokenizer;

	/** The operators. */
	private final Map<String, List<Operator>> operators;

	/** The constants. */
	private final Map<String, Constant> constants;

	/** The expression brackets. */
	private final Map<String, BracketPair> expressionBrackets;

	/**
	 * Instantiates a new abstract evaluator.
	 *
	 * @param parameters the parameters
	 */
	protected AbstractEvaluator(Parameters parameters) {
		ArrayList<String> tokenDelimitersBuilder = new ArrayList();
		this.operators = new HashMap();
		this.constants = new HashMap();
		this.expressionBrackets = new HashMap();
		for (BracketPair pair : parameters.getExpressionBrackets()) {
			this.expressionBrackets.put(pair.getOpen(), pair);
			this.expressionBrackets.put(pair.getClose(), pair);
			tokenDelimitersBuilder.add(pair.getOpen());
			tokenDelimitersBuilder.add(pair.getClose());
		}
		if (this.operators != null) {
			for (Operator ope : parameters.getOperators()) {
				tokenDelimitersBuilder.add(ope.getSymbol());
				List<Operator> known = (List) this.operators.get(ope.getSymbol());
				if (known == null) {
					known = new ArrayList();
					this.operators.put(ope.getSymbol(), known);
				}
				known.add(ope);
				if (known.size() > 1) {
					validateHomonyms(known);
				}
			}
		}
		if (parameters.getConstants() != null) {
			for (Constant constant : parameters.getConstants()) {
				this.constants.put(parameters.getTranslation(constant.getName()), constant);
			}
		}
		this.tokenizer = new Tokenizer(tokenDelimitersBuilder);
	}

	/**
	 * Evaluate.
	 *
	 * @param constant          the constant
	 * @param evaluationContext the evaluation context
	 * @return the t
	 */
	protected T evaluate(Constant constant, Object evaluationContext) {
		throw new RuntimeException("evaluate(Constant) is not implemented for " + constant.getName());
	}

	/**
	 * Evaluate.
	 *
	 * @param operator          the operator
	 * @param operands          the operands
	 * @param evaluationContext the evaluation context
	 * @return the t
	 */
	public T evaluate(Operator operator, Iterator<T> operands, Object evaluationContext) {
		throw new RuntimeException("evaluate(Operator, Iterator) is not implemented for " + operator.getSymbol());
	}

	/**
	 * Evaluate.
	 *
	 * @param expression the expression
	 * @return the t
	 */
	public T evaluate(String expression) {
		return evaluate(expression, null);
	}

	/**
	 * Evaluate.
	 *
	 * @param expression        the expression
	 * @param evaluationContext the evaluation context
	 * @return the t
	 */
	public T evaluate(String expression, Object evaluationContext) {
		Deque<T> values = new ArrayDeque();
		Deque<Token> stack = new ArrayDeque();
		Iterator<String> tokens = tokenize(expression);
		Token previous = null;
		while (tokens.hasNext()) {
			String strToken = (String) tokens.next();
			Token token = toToken(previous, strToken);
			if (token.isOpenBracket()) {
				stack.push(token);
				if (!this.expressionBrackets.containsKey(token.getBrackets().getOpen())) {
					throw new IllegalArgumentException("Invalid bracket in expression: " + strToken);
				}
			} else if (token.isCloseBracket()) {
				if (previous == null) {
					throw new IllegalArgumentException("expression can't start with a close bracket");
				}
				BracketPair brackets = token.getBrackets();

				boolean openBracketFound = false;
				while (!stack.isEmpty()) {
					Token sc = (Token) stack.pop();
					if (sc.isOpenBracket()) {
						if (sc.getBrackets().equals(brackets)) {
							openBracketFound = true;
							break;
						}
						throw new IllegalArgumentException(
								"Invalid parenthesis match " + sc.getBrackets().getOpen() + brackets.getClose());
					}
					output(values, sc, evaluationContext);
				}
				if (!openBracketFound) {
					throw new IllegalArgumentException("Parentheses mismatched");
				}
			} else if (token.isOperator()) {
				while (!stack.isEmpty()) {
					Token sc = (Token) stack.peek();
					if ((!sc.isOperator()) || (((!token.getAssociativity().equals(Operator.Associativity.LEFT))
							|| (token.getPrecedence() > sc.getPrecedence()))
							&& (token.getPrecedence() >= sc.getPrecedence()))) {
						break;
					}
					output(values, (Token) stack.pop(), evaluationContext);
				}
				stack.push(token);
			} else {
				if ((previous != null) && (previous.isLiteral())) {
					throw new IllegalArgumentException("A literal can't follow another literal");
				}
				output(values, token, evaluationContext);
			}
			previous = token;
		}
		while (!stack.isEmpty()) {
			Token sc = (Token) stack.pop();
			if ((sc.isOpenBracket()) || (sc.isCloseBracket())) {
				throw new IllegalArgumentException("Parentheses mismatched");
			}
			output(values, sc, evaluationContext);
		}
		if (values.size() != 1) {
			throw new IllegalArgumentException();
		}
		return values.pop();
	}

	/**
	 * Gets the arguments.
	 *
	 * @param values the values
	 * @param nb     the nb
	 * @return the arguments
	 */
	private Iterator<T> getArguments(Deque<T> values, int nb) {
		if (values.size() < nb) {
			throw new IllegalArgumentException();
		}
		LinkedList<T> result = new LinkedList();
		for (int i = 0; i < nb; i++) {
			result.addFirst(values.pop());
		}
		return result.iterator();
	}

	/**
	 * Gets the bracket pair.
	 *
	 * @param token the token
	 * @return the bracket pair
	 */
	private BracketPair getBracketPair(String token) {
		BracketPair result = (BracketPair) this.expressionBrackets.get(token);
		return result;
	}

	/**
	 * Gets the constants.
	 *
	 * @return the constants
	 */
	public Collection<Constant> getConstants() {
		return this.constants.values();
	}

	/**
	 * Gets the operators.
	 *
	 * @return the operators
	 */
	public Collection<Operator> getOperators() {
		ArrayList<Operator> result = new ArrayList();
		Collection<List<Operator>> values = this.operators.values();
		for (List<Operator> list : values) {
			result.addAll(list);
		}
		return result;
	}

	/**
	 * Guess operator.
	 *
	 * @param previous   the previous
	 * @param candidates the candidates
	 * @return the operator
	 */
	protected Operator guessOperator(Token previous, List<Operator> candidates) {
		int argCount = (previous != null) && ((previous.isCloseBracket()) || (previous.isLiteral())) ? 2 : 1;
		for (Operator operator : candidates) {
			if (operator.getOperandCount() == argCount) {
				return operator;
			}
		}
		return null;
	}

	/**
	 * Output.
	 *
	 * @param values            the values
	 * @param token             the token
	 * @param evaluationContext the evaluation context
	 */
	private void output(Deque<T> values, Token token, Object evaluationContext) {
		if (token.isLiteral()) {
			String literal = token.getLiteral();
			Constant ct = (Constant) this.constants.get(literal);
			T value = ct == null ? null : evaluate(ct, evaluationContext);
			if ((value == null) && (evaluationContext != null)
					&& ((evaluationContext instanceof AbstractVariableSet))) {
				value = (T) ((AbstractVariableSet) evaluationContext).get(literal);
			}
			values.push(value != null ? value : toValue(literal, evaluationContext));
		} else if (token.isOperator()) {
			Operator operator = token.getOperator();
			values.push(evaluate(operator, getArguments(values, operator.getOperandCount()), evaluationContext));
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Tokenize.
	 *
	 * @param expression the expression
	 * @return the iterator
	 */
	protected Iterator<String> tokenize(String expression) {
		return this.tokenizer.tokenize(expression);
	}

	/**
	 * To token.
	 *
	 * @param previous the previous
	 * @param token    the token
	 * @return the token
	 */
	private Token toToken(Token previous, String token) {
		if (this.operators.containsKey(token)) {
			List<Operator> list = (List) this.operators.get(token);
			return list.size() == 1 ? Token.buildOperator((Operator) list.get(0))
					: Token.buildOperator(guessOperator(previous, list));
		}
		BracketPair brackets = getBracketPair(token);
		if (brackets != null) {
			if (brackets.getOpen().equals(token)) {
				return Token.buildOpenToken(brackets);
			}
			return Token.buildCloseToken(brackets);
		}
		return Token.buildLiteral(token);
	}

	/**
	 * To value.
	 *
	 * @param paramString the param string
	 * @param paramObject the param object
	 * @return the t
	 */
	protected abstract T toValue(String paramString, Object paramObject);

	/**
	 * Validate homonyms.
	 *
	 * @param operators the operators
	 */
	protected void validateHomonyms(List<Operator> operators) {
		if (operators.size() > 2) {
			throw new IllegalArgumentException();
		}
	}
}
