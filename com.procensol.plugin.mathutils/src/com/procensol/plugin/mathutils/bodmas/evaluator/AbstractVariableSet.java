/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.mathutils.bodmas.evaluator;

// TODO: Auto-generated Javadoc
/**
 * The Interface AbstractVariableSet.
 *
 * @param <T> the generic type
 */
public abstract interface AbstractVariableSet<T> {

	/**
	 * Gets the.
	 *
	 * @param paramString the param string
	 * @return the t
	 */
	public abstract T get(String paramString);
}
